# Nextcloud server files

This builds Nextcloud server as package.

This just provides a package with server files (placed in ```/var/www/nextcloud```) similar to ```owncloud-files``` package (described [here](https://owncloud.org/blog/time-to-upgrade-to-owncloud-9-0/)) for keeping your installation base up to date. You need to take care of all the webserver and database stuff yourself.

Basically it's just re-packing the release archive and adding some meta information using [fpm](https://github.com/jordansissel/fpm/). You can check how it's done by browsing this repos file tree.

 information_source: The Nextcloud code itself isn't modified at all!

## Repo

You can find uploaded packages [here](http://repo.morph027.de/nextcloud.php). Upload is done by [gitlab-ci-aptly-cli](https://gitlab.com/morph027/gitlab-ci-aptly-cli/).

## Recommendations

### Updater

As of version 11, Nextcloud does have a builtin updater. If you want to rely on packages instead, you should not use it.

### Packages

* [Dotdeb repo for PHP7](https://www.dotdeb.org/instructions/)
* [MariaDB database server](https://downloads.mariadb.org/mariadb/repositories/#distro=Debian&distro_release=jessie--jessie&version=10.1)
