#!/bin/bash

set -e
set -x

_prepare() {
  apk --no-cache add build-base libffi-dev ruby-dev tar git curl
  gem install fpm --no-ri --no-rdoc
}

_fpm() {
  fpm -s tar -t deb -a all --name nextcloud-files --version "$CI_BUILD_TAG" --vendor Nextcloud --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" --description "Nextcloud server: a safe home for all your data - community-driven, free & open source" --url "https://nextcloud.com/" --deb-user "www-data" --deb-group "www-data" --prefix "/var/www" "nextcloud-${CI_BUILD_TAG}.tar.bz2"
  fpm -s tar -t deb -a all --name nextcloud-files-readonly --version "$CI_BUILD_TAG" --vendor Nextcloud --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" --description "Nextcloud server: a safe home for all your data - community-driven, free & open source" --url "https://nextcloud.com/" --deb-user "root" --deb-group "www-data" --prefix "/var/www" "nextcloud-${CI_BUILD_TAG}.tar.bz2"
}

_build_from_tar() {
  curl -sLO "$RELEASE_URL/nextcloud-${CI_BUILD_TAG}.tar.bz2"
  curl -sLO "$RELEASE_URL/nextcloud-${CI_BUILD_TAG}.tar.bz2.sha512"
  sha512sum -c "nextcloud-${CI_BUILD_TAG}.tar.bz2.sha512"
  _fpm
}

which fpm || _prepare

_build_from_tar
